#!/bin/sh

# Sun JARs
mvn install:install-file -DgroupId=javax.media -Dversion=1.1.3 -Dpackaging=jar \
    -DartifactId=jai_imageio -Dfile=src/main/webapp/WEB-INF/lib/jai_imageio.jar

mvn install:install-file -DgroupId=javax.media -Dversion=1.1.3 -Dpackaging=jar \
    -DartifactId=jai_core -Dfile=src/main/webapp/WEB-INF/lib/jai_core.jar

mvn install:install-file -DgroupId=javax.media -Dversion=1.1.3 -Dpackaging=jar \
    -DartifactId=jai_codec -Dfile=src/main/webapp/WEB-INF/lib/jai_codec.jar

mvn install:install-file -DgroupId=com.sun.media -Dversion=1.1 -Dpackaging=jar \
    -DartifactId=jai_imageio -Dfile=src/main/webapp/WEB-INF/lib/jai_imageio.jar

mvn install:install-file -DgroupId=com.sun.media -Dversion=1.1 -Dpackaging=jar \
    -DartifactId=jai_core -Dfile=src/main/webapp/WEB-INF/lib/jai_core.jar

mvn install:install-file -DgroupId=com.sun.media -Dversion=1.1 -Dpackaging=jar \
    -DartifactId=jai_codec -Dfile=src/main/webapp/WEB-INF/lib/jai_codec.jar

mvn install:install-file -Dfile=src/main/webapp/WEB-INF/lib/commons-codec-1.4.jar \
    -DgroupId=org.apache.commons.codec -DartifactId=commons-codec -Dversion=1.4 -Dpackaging=jar
